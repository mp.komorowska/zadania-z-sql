-- a. Napisz kwerendę, która zwróci wszystkich klientów z zamówieniami zrealizowanymi w dniu 2008-01-08

SELECT c.CUST_CODE, ORD_DATE
FROM customer c
JOIN orders o
ON c.CUST_CODE = o.CUST_CODE
WHERE ORD_DATE = '2008-01-08';

-- b. Napisz kwerendę, która zwróci listę agentów oraz klientów wraz z ich obszarem działalności, którzy należą do tego samego obszaru
 SELECT AGENT_NAME, CUST_NAME, c.WORKING_AREA
 FROM agents a
 JOIN customer c
 ON a.AGENT_CODE = c.AGENT_CODE
 WHERE a.WORKING_AREA = c.WORKING_AREA;
 
-- INNER JOIN
-- a. Wyszukaj listę klientów, którzy podjęli współpracę z agentami spoza swojego obszaru działalności oraz tymi, których prowizja jest powyżej 12%; kolumna z nazwą pośrednika powinna mieć nazwę „Salesman”
 SELECT 
	AGENT_NAME as Salesman, 
	CUST_NAME, 
	c.WORKING_AREA,
	a.WORKING_AREA,
  	a.COMMISSION
 FROM agents a
 JOIN customer c
 ON a.AGENT_CODE = c.AGENT_CODE
 WHERE COMMISSION > 0.12 OR c.WORKING_AREA != a.WORKING_AREA;

-- b. Wyszukaj szczegóły dot. Zamówień: nr zamówienia, datę, kwotę, klienta (nazwa kolumny powinna być „Customer Name”) oraz agenta (w tym wypadku nazwa kolumny to „Salesman”), który pracuje dla tego klienta oraz jego prowizję od zamówienia
 
 SELECT
	o.ORD_NUM,
	o.ORD_DATE,
	o.ORD_AMOUNT,
	c.CUST_NAME as 'Customer name',
    a.AGENT_NAME as 'Salesman',
	a.COMMISSION
FROM orders o
JOIN customer c
ON o.CUST_CODE = c.CUST_CODE
JOIN agents a
ON a.AGENT_CODE = o.AGENT_CODE;
 
 
 
-- LEFT JOIN
-- a. Wyszukaj listę - posortowaną rosnąco wg kodu klienta (jego id) – klientów pracujących zarówno indywidualnie, jak również za pośrednictwem pośredników (nazwij kolumnę zawierającą nazwy pośredników jako „Salesman”), wyszukaj również ich obszar działalności
 
SELECT 
	c.CUST_NAME,
	c.AGENT_CODE, 
	a.AGENT_CODE, 
	a.AGENT_NAME
FROM customer c
LEFT JOIN agents a
ON c.AGENT_CODE = a.AGENT_CODE
ORDER BY c.CUST_CODE ASC;

 
-- RIGHT JOIN
-- a. Wyszukaj listę pośredników pracujących dla jednego bądź więcej klientów lub takich, którzy jeszcze nie podjęli współpracy z żadnym klientem, posortuj listę rosnąco wg kolumny kodu pośrednika (agenta); nazwij kolumnę zawierającą nazwy pośredników jako „Salesman”, wyszukaj również ich obszar działalności
 
 SELECT
	a.AGENT_NAME AS Salesman,
    a.AGENT_CODE,
    a.WORKING_AREA,
    c.CUST_NAME
FROM customer c
RIGHT JOIN agents a
ON a.AGENT_CODE = c.AGENT_CODE
ORDER BY a.AGENT_CODE ASC;
 
-- CROSS JOIN
-- a. Wyszukaj iloczyn kartezjański pośredników oraz klientów, w taki sposób, że każdy pośrednik będzie widoczny dla wszystkich klientów i vice versa.

SELECT 
	a.AGENT_NAME,
    c.CUST_NAME
FROM customer c
CROSS JOIN agents a;

-- b. Wyszukaj iloczyn kartezjański pośredników oraz klientów, w taki sposób, że każdy pośrednik będzie widoczny dla wszystkich klientów i vice versa, ale tylko jeżeli pośrednik jest z tego samego obszaru co klient.
SELECT 
	a.AGENT_NAME,
    c.CUST_NAME
FROM customer c
CROSS JOIN agents a
WHERE c.WORKING_AREA = a.WORKING_AREA;

-- c. Wyszukaj iloczyn kartezjański pośredników oraz klientów, w taki sposób, że każdy pośrednik będzie widoczny dla wszystkich klientów i vice versa, ale tylko jeżeli pośrednik jest z innego obszaru niż klient, a klient posiada swoją własną ocenę (‘grade’)
 SELECT 
	a.AGENT_NAME,
    c.CUST_NAME
FROM customer c
CROSS JOIN agents a
WHERE c.WORKING_AREA != a.WORKING_AREA AND c.grade IS NOT NULL;



-- UNION
-- a. Wyszukaj wszystkich pośredników (ich kod – nazwij kolumne „ID” oraz nazwa agenta); przypisz im wartość ‘Salesman’ w kolumnie o nazwie „Rodzaj”) oraz klientów – ich ID i nazwa (wartość ‘Customer’ w kolumnie „Rodzaj”) zlokalizowanych w Londynie
SELECT 
	AGENT_CODE AS ID,
    AGENT_NAME AS nazwa_agenta,
    WORKING_AREA,
    'Salesman' as 'Rodzaj'
    FROM agents
    WHERE WORKING_AREA = 'London'
UNION
SELECT 
	CUST_CODE AS ID,
	CUST_NAME AS nazwa_klienta,
	WORKING_AREA, 
	'Customer' AS 'Rodzaj'
	FROM customer
	WHERE WORKING_AREA = 'London';

-- b. Napisz kwerendę, która zwróci raport pokazujący, który pośrednik przyjął największe i najmniejsze zamówienia na każdy dzień

SELECT 
	a.AGENT_NAME,
    MAX(o.ORD_AMOUNT),
    MIN(o.ORD_AMOUNT),
	o.ORD_DATE
FROM orders o
INNER JOIN agents a
WHERE a.AGENT_CODE = o.AGENT_CODE
GROUP BY ORD_DATE
ORDER BY 2 DESC;


-- WITH
-- a. Wyszukaj pośrednika w Londynie z prowizją powyżej 14% - nazwij widok „londonstaff”

WITH londonstaff AS (
	SELECT * 
    FROM agents
)
SELECT *
FROM londonstaff
WHERE COMMISSION > 0.14; 

-- b. Uwórtz widok „gradecount”, aby uzyskać liczbę klientów dla każdej oceny (‘grade’)

WITH gradecount AS (
	SELECT *
    FROM customer
)
SELECT COUNT(CUST_CODE), GRADE
FROM gradecount
GROUP BY GRADE;

-- c. Utwórz widok (o nazwie „total_per_dzien”) do śledzenia liczby klientów, średniej kwoty zamówień oraz ogólnej sumy zamówień na każdy dzień

CREATE VIEW total_per_dzien AS 
SELECT 
	COUNT(CUST_CODE) AS 'liczba klientów',
	AVG(ORD_AMOUNT) AS 'średnia kwota zamówień',
    SUM(ORD_NUM) AS 'suma zamówień'
FROM orders;
