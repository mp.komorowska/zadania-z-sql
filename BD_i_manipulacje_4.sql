CREATE DATABASE pracownicy5;

-- 2
CREATE TABLE pracownicy1 (
	ID INTEGER PRIMARY KEY,
    Imie TEXT,
    Nazwisko TEXT,
    Wiek INTEGER CHECK (Wiek>=18),
    Kurs TEXT
    );

-- 3
INSERT INTO pracownicy1 (ID, Imie, Nazwisko, Wiek, Kurs)
VALUES
	(1,"Anna","NOWAK",34,"DS"),
	(2,"Roman","KOWALSKI",42,"DS"),
	(3,"Tomasz","WIŚNIEWSKI",33,"DS"),
	(4,"Agata" ,"WÓJCIK",43,"DS"),
	(5,"Elżbieta","KOWALCZYK",28,"Java"),
	(6,"Przemysław","KOWALCZYK",34,"Java"),
	(7,"Robert","kOWALCZYK",35,"Java"),
	(8,"Radosław","ZIELIŃSKI",38,"Java"),
	(9,NULL,"WOŹNIAK",26,"Java"),
	(10,"Robert","SZYMAŃSKI",34,"Java"),
	(11,"Radosław","DĄBROWSKI","35",NULL),
	(12,"Robert","KOZŁOWSKI",NULL,"UX"),
	(13,"Joanna","MAZUR",26,"UX"),
	(14,"Radosław","JANKOWSKI",27,"UX"),
	(15,"Patryk","LEWANDOWSKI",28,"Tester"),
	(16,NULL,"ZIELIŃSKI",28,"Tester"),
	(17,"Andrzej","WOŹNIAK",31,"Tester"),
	(18,"Andrzej","LEWANDOWSKI",30,"Tester"),
	(19,"Roman","KOWALCZYK",39,"Tester"),
	(20,"Ewa","WOŹNIAK",31,"Tester");
    
-- 4
SELECT * FROM pracownicy1
WHERE Imie = "Anna";

-- 5
SELECT * FROM pracownicy1
WHERE Imie IS NULL;

-- 6
SELECT kurs FROM pracownicy1
WHERE wiek BETWEEN 30 AND 40;

-- 7
SELECT wiek FROM pracownicy1
WHERE ID BETWEEN 1 AND 7;

-- 8
SELECT * FROM pracownicy1
WHERE wiek IS NULL;

-- 9
ALTER TABLE pracownicy1
RENAME COLUMN Kurs TO Szkolenie;
