-- a. Podaj średnią wieku. Podaj zaokrąglony wynik do dwóch miejsc po przecinku.
SELECT
	ROUND(AVG(AGE))
	FROM aggregates.batman;

-- b. Wyświetl wartość bezwzględną liczby -3.
SELECT ABS(-3);
-- c. Wyświetl wynik mnożenie 2 * 3
SELECT 2*3;
-- d. Wyświetl wynik dzielenia 6 przez 
SELECT 6/2;
