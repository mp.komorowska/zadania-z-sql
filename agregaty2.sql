    
-- a. Wyświetl imię i nazwisko w jednej kolumnie. Kolumnie nadaj alias „Pracownik”
SELECT CONCAT(first_name," ", last_name) AS "Pracownik"
FROM aggregates.batman;

-- b. Złącz kolumny gift i price tak, aby powstał napis: „rower – cena: ”. Kolumnie nadaj alias: cennik.
SELECT CONCAT(gift,"-", price) AS "cennik"
FROM aggregates.batman;

-- c. Ponownie wykonaj zadanie 2b – doklej imię i nazwisko pracownika
SELECT CONCAT(first_name," ", last_name," ", gift,"-", price) AS "cennik"
FROM aggregates.batman;

-- d. Wyświetl prezenty. Ich nazwy mają być wyświetlane wielkimi literami.
SELECT UPPER(gift)
FROM aggregates.batman;

-- e. Wyświetl imiona. Mają być wyświetlane małymi literami.
SELECT LOWER(first_name)
FROM aggregates.batman;

-- f. Wyświetl nazwiska oraz dodatkową kolumnę „długość” zawierającą informację o liczbie liter w nazwisku.
SELECT
last_name AS nazwisko,
CHAR_LENGTH(last_name) AS długość
FROM aggregates.batman;

-- g. Wyświetl pierwsze dwie litery imion.
SELECT SUBSTR(first_name, 1,2)
FROM aggregates.batman;

-- h. Wyświetl imię, nazwisko i login użytkownika w formacie: mała litera imienia + 3 pierwsze, małe litery nazwiska.

SELECT
first_name as imię,
last_name as nazwisko,
LOWER(CONCAT(SUBSTR(first_name, 1,1), SUBSTR(last_name, 1,3))) as login
From aggregates.batman;
