-- a. Policz, ile jest rekordów w tabeli (po id).
SELECT COUNT(id)
FROM aggregates.batman;

-- b. Policz, ile jest rekordów z wiekiem.
SELECT COUNT(age)
FROM aggregates.batman;

-- c. Ilu mężczyzn ma więcej niż 40 lat?
SELECT COUNT(age)
FROM aggregates.batman
WHERE age>40 AND sex='M';

-- d. Oblicz sumę wszystkich cen.
SELECT SUM(price)
FROM aggregates.batman;

-- e. Oblicz łączny wiek kobiet.
SELECT SUM(age)
FROM aggregates.batman
WHERE sex='F';

-- f. Podaj łączną wartość komputera i okna.
SELECT SUM(price)
FROM aggregates.batman
WHERE gift IN ('komputer','okno');

-- g. Podaj maksymalną cenę.
SELECT MAX(price)
FROM aggregates.batman;

-- h. Podaj najpóźniejszą datę
SELECT MAX(start_date)
FROM aggregates.batman;

-- i. Podaj nazwisko rozpoczynające się literą, która jest najbliżej końca alfabetu
SELECT MAX(last_name)
FROM aggregates.batman;

-- j. Podaj minimalną cenę.
SELECT MIN(price)
FROM aggregates.batman;

-- k. Podaj najwcześniejszą datę
SELECT MIN(start_date)
FROM aggregates.batman;

-- l. Podaj nazwisko rozpoczynające się literą, która jest najbliżej początku alfabetu
SELECT MIN(last_name)
FROM aggregates.batman;

-- m. Jednocześnie podaj cenę minimalną oraz maksymalną. Nadaj odpowiednie aliasy.

SELECT MIN(price) AS 'cena minimalna', 
	MAX(price) AS 'cena maksymalna'
	FROM aggregates.batman;
        
-- n. Oblicz różnicę pomiędzy ceną maksymalną, a minimalną.
SELECT MAX(price)-MIN(price)
FROM aggregates.batman;

-- o. Oblicz średnią wieku.
SELECT ROUND(AVG(age))
FROM aggregates.batman;

-- p. Oblicz średnią wieku za pomocą COUNT oraz SUM.
SELECT SUM(age)/COUNT(age)
FROM aggregates.batman;

-- q. Oblicz średni wiek dla kobiet oraz średni wiek dla mężczyzn.
SELECT AVG(age) AS 'średni wiek kobiet'
FROM aggregates.batman
WHERE sex = 'F';

SELECT AVG(age) AS 'średni wiek mężczyzn'
FROM aggregates.batman
WHERE sex = 'M';
