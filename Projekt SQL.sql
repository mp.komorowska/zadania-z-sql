-- 1
CREATE DATABASE Sklep_odziezowy;

-- 2
CREATE TABLE producenci(
	id_producenta INTEGER PRIMARY KEY,
    nazwa_producenta TEXT DEFAULT NULL,
    adres_producenta TEXT DEFAULT NULL,
    nip_producenta TEXT CHECK(LENGTH(nip_producenta)=10),
    data_podpisania_umowy_z_producentem DATE DEFAULT NULL
    );

-- 3
CREATE TABLE produkty(
	id_produktu INTEGER PRIMARY KEY,
    id_producenta INTEGER NOT NULL,
    nazwa_produktu TEXT DEFAULT NULL,
    opis_produktu TEXT DEFAULT NULL,
    cena_netto_zakupu decimal(6,2) DEFAULT NULL,
    cena_brutto_zakupu decimal(6,2) DEFAULT NULL,
    cena_netto_sprzedazy decimal(6,2) DEFAULT NULL,
    cena_brutto_sprzedazy decimal(6,2) DEFAULT NULL,
    procent_VAT_sprzedazy decimal(3,2) DEFAULT NULL
    );
    
-- 4
CREATE TABLE zamowienia(
	id_zamowienia INTEGER PRIMARY KEY,
	id_klienta INTEGER NOT NULL,
	id_produktu INTEGER NOT NULL,
	data_zamowienia DATE DEFAULT NULL
);

-- 5
CREATE TABLE klienci(
	id_klienta INTEGER PRIMARY KEY,
	id_zamowienia INTEGER NOT NULL,
	imie TEXT DEFAULT NULL, 
	nazwisko TEXT DEFAULT NULL,
	adres TEXT DEFAULT NULL
);

-- 6 
ALTER TABLE produkty
ADD FOREIGN KEY (id_producenta) REFERENCES producenci(id_producenta);

ALTER TABLE zamowienia
ADD FOREIGN KEY (id_produktu) REFERENCES produkty(id_produktu);

ALTER TABLE zamowienia
ADD FOREIGN KEY (id_klienta) REFERENCES klienci(id_klienta);

-- 7
INSERT INTO producenci (id_producenta, nazwa_producenta, adres_producenta, nip_producenta, data_podpisania_umowy_z_producentem)
    VALUES
		("100001", "Zara", "Słoneczna 15", "1234563218", "2015-05-12"),
        ("100002", "HM", "Norwida 12A", "1834565218", "2008-04-30"),
        ("100003", "Ikea", "Grochowska 234", "5234546225", "2004-01-18"),
        ("100004", "Jysk", "Poleczki 1", "9524213088", "2017-03-02");

INSERT INTO produkty (id_produktu, id_producenta, nazwa_produktu, opis_produktu, cena_netto_zakupu, cena_brutto_zakupu, cena_netto_sprzedazy, cena_brutto_sprzedazy, procent_VAT_sprzedazy)
    VALUES
		("200001", "100001", "Spódnica", "Ubranie damskie", "50.00", "65.00","58.99", "72.99", "0.23"),
        ("200002", "100001", "Spodnie", "Ubranie męskie", "60.00", "75.00","78.99", "90.99", "0.23"),
        ("200003", "100001", "Sukienka", "Ubranie damskie", "100.00", "123.00","159.99", "189.99", "0.23"),
        ("200004", "100001", "Bluzka", "Ubranie damskie", "30.00", "45.00","59.99", "80.99", "0.23"),
		("200005", "100001", "Czapka", "Nakrycie głowy", "35.00", "55.00","45.99", "60.99", "0.23"),
        ("200006", "100002", "Naszyjnik", "Biżuteria", "50.00", "65.00","150.99", "190.99", "0.23"),
        ("200007", "100002", "Botki", "Obuwie", "250.00", "265.00","279.99", "299.99", "0.23"),
        ("200008", "100002", "Kapelusz", "Nakrycie głowy", "80.00", "95.00","129.99", "149.99", "0.23"),
        ("200009", "100002", "Skarpetki", "Ubranie damskie", "20.00", "35.00","40.99", "50.00", "0.23"),
        ("200010", "100002", "Marynarka", "Ubranie damskie", "250.00", "265.00","279.99", "299.99", "0.23"),
        ("200011", "100003", "Łóżko", "Meble sypialnia", "1050.00", "1099.99","1205.99", "1499.99", "0.23"),
        ("200012", "100003", "Biurko", "Meble biurowe", "650.00", "685.00","850.00", "999.99", "0.23"),
        ("200013", "100003", "Szafa", "Przechowywanie", "3000.00", "3250.00","3800.00", "4299.99", "0.23"),
        ("200014", "100003", "Pojemniki", "Przechowywanie", "50.00", "65.00","58.99", "72.99", "0.23"),
        ("200015", "100003", "Fotel", "Meble salon", "550.00", "565.00","699.99", "750.00", "0.23"),
        ("200016", "100004", "Dywan", "Dekoracja", "150.00", "190.00","180.00", "199.99", "0.23"),
        ("200017", "100004", "Ręczniki", "Dekoracja", "50.00", "65.00","99.99", "129.99", "0.23"),
        ("200018", "100004", "Materac", "Meble sypialnia", "1500.00", "1750.00","1750.99", "1999.99", "0.23"),
        ("200019", "100004", "Poduszki", "Dekoracja", "80.00", "100.00","120.00", "152.99", "0.23"),
        ("200020", "100004", "Koc", "Dekoracja", "30.00", "55.00","70.99", "85.99", "0.23");
        
INSERT INTO zamowienia (id_zamowienia, id_klienta, id_produktu, data_zamowienia)
    VALUES
		("300001", "400008","200008", "2010-05-12"),
		("300002", "400002","200014", "2015-01-15"),
        ("300003", "400007","200018", "2009-04-15"),
        ("300004", "400004","200004", "2009-04-15"),
		("300005", "400009","200009", "2012-11-05"),
        ("300006", "400006","200001", "2009-01-31"),
        ("300007", "400003","200011", "2018-08-05"),
        ("300008", "400001","200019", "2018-09-09"),
		("300009", "400005","200003", "2019-01-13"),
		("300010", "400010","200007", "2019-01-13");
        
  INSERT INTO klienci (id_klienta, id_zamowienia, imie, nazwisko, adres)
    VALUES
		("400001", "300008","Anna", "Kowalska", "Zamojska 28/2"),
        ("400002", "300002","Maria", "Kowalska", "Długa 5"),
        ("400003", "300007","Adam", "Nowak", "Radosna 21/88"),
        ("400004", "300004","Mateusz", "Wiśniewski", "Heroldów 21/3"),
		("400005", "300009","Damian", "Kowal", "Chopina 45"),
        ("400006", "300006","Joanna", "Rabata", "Dolna 56/123"),
        ("400007", "300003","Daria", "Chrząszcz", "Warszawska 12C"),
        ("400008", "300001","Robert", "Zduński", "Krótka 2"),
        ("400009", "300005","Adrianna", "Robak", "Żbikowska 32/67"),
        ("400010", "300010","Roman", "Wroński", "Zamojska 52");
        
-- 8.
SELECT *
FROM produkty p
JOIN producenci a
ON p.id_producenta = a.id_producenta
WHERE p.id_producenta = "100001";

-- 9. 
SELECT *
FROM produkty p
JOIN producenci a
ON p.id_producenta = a.id_producenta
WHERE p.id_producenta = "100001"
ORDER BY nazwa_produktu ASC;

-- 10.
SELECT AVG(cena_brutto_sprzedazy) AS "średnia cena produktu"
FROM produkty
WHERE id_producenta = "100001";

-- 11. 
SELECT nazwa_produktu,
	CASE
		WHEN cena_brutto_sprzedazy< (SELECT AVG(cena_brutto_sprzedazy) FROM produkty) THEN "Tanie"
        WHEN cena_brutto_sprzedazy> (SELECT AVG(cena_brutto_sprzedazy) FROM produkty) THEN "Drogie"
	END as Grupa_cenowa
FROM produkty
ORDER BY 2;

-- 12. 
SELECT nazwa_produktu
FROM zamowienia z
JOIN produkty p
ON z.id_produktu = p.id_produktu;

-- 13.
SELECT nazwa_produktu
FROM zamowienia z
JOIN produkty p
ON z.id_produktu = p.id_produktu
LIMIT 5;

-- 14.
SELECT SUM(cena_netto_sprzedazy) AS "Suma sprzedaży netto", 
	   SUM(cena_brutto_sprzedazy) AS "Suma sprzedaży brutto"
FROM zamowienia z
JOIN produkty p
ON z.id_produktu = p.id_produktu;

-- 15.
SELECT id_zamowienia, nazwa_produktu, data_zamowienia
FROM zamowienia z
JOIN produkty p
ON z.id_produktu = p.id_produktu
ORDER BY data_zamowienia ASC;

-- 16.
SELECT * FROM produkty
WHERE id_produktu IS NULL
	  OR id_producenta IS NULL
      OR nazwa_produktu IS NULL
      OR opis_produktu IS NULL
      OR cena_netto_zakupu IS NULL
      OR cena_brutto_zakupu IS NULL
      OR cena_netto_sprzedazy IS NULL
      OR cena_brutto_sprzedazy IS NULL
      OR procent_VAT_sprzedazy IS NULL;

-- 17.

WITH naj_sprzedawany AS (
SELECT id_produktu,
		COUNT(*) AS liczba_zamowien
FROM zamowienia
GROUP BY id_produktu
),
max_naj_sprzedawany AS (
SELECT MAX(liczba_zamowien) max_liczba_zamowien
FROM naj_sprzedawany)

SELECT n.id_produktu, p.cena_brutto_sprzedazy
FROM naj_sprzedawany n
JOIN produkty p ON n.id_produktu = p.id_produktu
WHERE liczba_zamowien = (SELECT max_liczba_zamowien FROM max_naj_sprzedawany);


-- 18.   
WITH naj_zamowien AS (
SELECT data_zamowienia, 
	   COUNT(data_zamowienia)  AS liczba_zamowien
FROM zamowienia 
GROUP BY data_zamowienia
),
max_liczba_zam AS (
	SELECT MAX(liczba_zamowien) AS max_zamowien
	FROM naj_zamowien)
    
SELECT data_zamowienia, liczba_zamowien
FROM naj_zamowien
WHERE liczba_zamowien = (SELECT max_zamowien FROM max_liczba_zam);     
        
        
