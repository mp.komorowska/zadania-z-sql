-- 18.   
SELECT z.data_zamowienia, liczba_zamowien
FROM (
	SELECT data_zamowienia, COUNT(data_zamowienia)  AS liczba_zamowien
	FROM zamowienia 
	GROUP BY data_zamowienia
	ORDER BY liczba_zamowien DESC) z
JOIN (
	SELECT data_zamowienia, max(liczba_zamowien) AS max_zamowien
	FROM (
		SELECT data_zamowienia, COUNT(data_zamowienia)  AS liczba_zamowien
		FROM zamowienia 
		GROUP BY data_zamowienia
		ORDER BY liczba_zamowien DESC
	 ) z2 
) z3
ON z.liczba_zamowien = z3.max_zamowien
