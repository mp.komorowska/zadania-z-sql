-- a. Rozłóż datę pokazaną w kolumnie start_date na trzy kolumny: rok, miesiąc, dzień.
SELECT
    SUBSTR(start_date, 1,4) as rok,
    SUBSTR(start_date, 6,7) as miesiąc,
    SUBSTR(start_date, 9,10) as dzień
    FROM aggregates.batman;

-- b. Wyświetl kolumnę start_date oraz kolumnę final_date, która wyświetli datę o 3 dni późniejszą, niż data w start_date.
SELECT
    start_date,
    DATE_ADD(start_date, INTERVAL +3 DAY) AS final_date
    FROM aggregates.batman;

-- c. Wyświetl dzisiejszą datę.
SELECT CURDATE();

-- d. Wyświetl nazwę obecnego miesiąca.
SELECT MONTHNAME(CURDATE());

-- e. Wyświetl start_date, a następnie kolumny określające: numer tygodnia, nazwę miesiąca, kwartał, numer dnia w roku.

SELECT
    start_date,
    WEEK(start_date) as 'numer tygodnia',
    MONTHNAME(start_date) as 'nazwa miesiąca',
    UARTER(start_date) as kwartał,
    DAYOFYEAR(start_date) as 'dzień roku'
    FROM aggregates.batman;
