CREATE DATABASE pracownicy3;

CREATE TABLE pracownicy3 (
	id INTEGER PRIMARY KEY,
    imie TEXT,
    nazwisko TEXT,
    wiek INTEGER,
    kurs TEXT
    );
-- 3
INSERT INTO pracownicy3 (id, imie, nazwisko, wiek, kurs)
VALUES
	(1 ,'Anna'      ,'NOWAK'       ,34    ,'DS.'),
	(2 ,'Roman'     ,'KOWALSKI'    ,42    ,'DS.'),
	(3 ,'Tomasz'    ,'WIŚNIEWSKI'  ,33    ,'DS.'),
	(4 ,'Agata'     ,'WÓJCIK'      ,43    ,'DS.'),
	(5 ,'Elżbieta'  ,'KOWALCZYK'   ,28    ,'Java'),
	(6 ,'Przemysław', NULL         ,34    ,'Java'),
	(7 ,'Robert'    , NULL         ,35    ,'Java'),
	(8 ,'Radosław'  ,'ZIELIŃSKI'   ,38    ,'Java'),
	(9 ,NULL        ,'WOŹNIAK'     ,26    ,'Java'),  
	(10,'Robert'    ,'SZYMAŃSKI'   ,34    ,'Java'),
	(11,'Radosław'  ,'DĄBROWSKI'   ,35    , NULL),
	(12,'Robert'    ,'KOZŁOWSKI'   ,NULL  ,'UX'),
	(13,'Joanna'    ,'MAZUR'       ,26    ,'UX'),
	(14,'Radosław'  ,'JANKOWSKI'   ,27    ,'UX'),
	(15,'Patryk'    ,'LEWANDOWSKI' ,28    ,'Tester'),
	(16, NULL       ,'ZIELIŃSKI'   ,28    ,'Tester'),
	(17,'Andrzej'   ,'WOŹNIAK'     ,31    ,'Tester'),
	(18,'Andrze'    ,'LEWANDOWSKI' ,30    ,'Tester'),
	(19,'Roman'     , NULL         ,39    ,'Tester'),
	(20,'Ewa'       ,'WOZNIAK'     ,31    ,'Tester');
    
-- 4
SELECT * FROM pracownicy3
WHERE wiek=28;
  
-- 5 
SELECT * FROM pracownicy3
WHERE wiek <=30;
 
-- 6
SELECT * FROM pracownicy3
WHERE nazwisko LIKE "%SKI%";

-- 7
SELECT * FROM pracownicy3
WHERE id IN (1,4,7,14,18,20); 

-- 8
SELECT * FROM pracownicy3
WHERE id IS NOT NULL 
	AND imie IS NOT NULL
	AND nazwisko IS NOT NULL 
	AND wiek IS NOT NULL 
	AND kurs IS NOT NULL;

-- 9
SELECT * FROM pracownicy3
WHERE NOT kurs = "DS.";
