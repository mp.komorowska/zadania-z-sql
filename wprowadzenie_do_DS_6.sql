-- 1
CREATE DATABASE moje_zainteresowania;
-- 2
CREATE TABLE zainteresowania (
    id INTEGER UNIQUE,
    nazwa TEXT,
    opis TEXT,
    data_realizacji DATE
);
-- 3
INSERT INTO zainteresowania (nazwa, opis)
VALUES ("gotowanie", "przygotowywanie nowych potraw"),
    ("bieganie", "bieganie po parku wieczorem"),
    ("nauka", "uczenie się nowych rzeczy"),
    ("sport", "ogladanie meczy tenisowych"),
    ("muzyka", "granie na gitarze");
-- 4 Error Code: 1364. Field 'id' doesn't have a default value - nie da się wyświetlić tabeli bez ID
-- 5
INSERT INTO zainteresowania (id, nazwa, opis, data_realizacji)
VALUES (6, "makijaż", "charakteryzacja", "2024-05-12");

SELECT *
FROM zainteresowania;
-- 6
UPDATE zainteresowania 
SET 
    id = 4,
    data_realizacji = '2022-07-08'
where nazwa = 'sport';

-- 7
SELECT *
FROM zainteresowania;

-- 8
DELETE from zainteresowania
WHERE id is NULL;

-- 9
SELECT *
FROM zainteresowania;
