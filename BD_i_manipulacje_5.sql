-- 2
CREATE TABLE pracownicy (
	ID INTEGER PRIMARY KEY,
    Imie TEXT,
    Nazwisko TEXT,
    Wiek INTEGER CHECK (Wiek>=18),
    Kurs TEXT
    );

-- 3
INSERT INTO pracownicy (ID, Imie, Nazwisko, Wiek, Kurs)
VALUES
	(1, "Anna","NOWAK",34,"DS"),
	(2, "Roman","KOWALSKI",42,"DS"),
	(3, "Tomasz","WIŚNIEWSKI",33,"DS"),
	(4, "Anna",NULL,43,"DS"),
	(5, "Elżbieta","KOWALCZYK",NULL,"Tester"),
	(6, "Anna","KOWALCZYK",NULL,"Java"),
	(7, "Robert","KOWALCZYK",NULL,"Java"),
	(8, "Radosław","ZIELIŃSKI",NULL,"Java"),
	(9, "Robert","WOŹNIAK",NULL,"Java"),
	(10,"Robert","SZYMAŃSKI",34,"Tester"),
	(11,"Radosław","DĄBROWSKI",35,NULL),
	(12,"Robert","KOZŁOWSKI",NULL,"UX"),
	(13,"Joanna","MAZUR",26,"UX"),
	(14,"Radosław","JANKOWSKI",27,"UX"),
	(15,"Patryk","LEWANDOWSKI",28,NULL),
	(16,NULL,"ZIELIŃSKI",28,NULL),
	(17,"Andrzej","WOŹNIAK",31,NULL),
	(18,"Andrze","LEWANDOWSKI",30,NULL),
	(19,"Roman","KOWALCZYK",39,NULL),
	(20,"Ewa","WOŹNIAK",31,NULL);
    
-- 4
SELECT DISTINCT Imie FROM pracownicy;

-- 5
SELECT DISTINCT Nazwisko FROM pracownicy;

-- 6
SELECT Kurs FROM pracownicy
WHERE Nazwisko = "KOWALCZYK";

-- 7
SELECT * FROM pracownicy
WHERE Wiek IS NULL;

-- 8
SELECT Wiek FROM pracownicy
WHERE Imie = "Patryk";

-- 9
ALTER TABLE pracownicy
RENAME TO Mentorzy;
