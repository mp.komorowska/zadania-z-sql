-- 1 
 DATABASE pracownicy2;
-- 2 
CREATE TABLE pracownicy2 (
	ID INTEGER PRIMARY KEY,
    Imie TEXT,
    Nazwisko TEXT,
    Wiek INTEGER NOT NULL,
    Kurs TEXT
    );
    
-- 3     
INSERT INTO pracownicy2 (ID, Imie, Nazwisko, Wiek, Kurs)
VALUES
	 (1 ,'Anna'       ,'NOWAK'       ,  34 ,'DS.'),
	 (2 ,'Roman'      ,'KOWALSKI'    ,  42 ,'DS.'),
	 (3 ,'Tomasz'     ,'WIŚNIEWSKI'  ,  33 ,'DS.'),
	 (4 ,'Agata'      ,'WÓJCIK'      ,  43 ,'DS.'),
	 (5 ,'Elżbieta'   ,'KOWALCZYK'   ,  28 ,'Java'),
	 (6 ,'Przemysław' ,'KAMIŃSKI'    ,  34 ,'Java'),
	 (7 ,'Robert'     ,'LEWANDOWSKI' ,  35 ,'Java'),
	 (8 ,'Radosław'   ,'ZIELIŃSKI'   ,  38 ,'Java'),
	 (9 ,'Anna'       ,'WOŹNIAK'     ,  26 ,'Java'),
	(10 ,'Robert'     ,'SZYMAŃSKI'   ,  34 ,'Java'),
	(11 ,'Radosław'   ,'DĄBROWSKI'   ,  35 ,'UX'),
	(12 ,'Robert'     ,'KOZŁOWSKI'   ,  38 ,'UX'),
	(13 ,'Joanna'     ,'MAZUR'       ,  26 ,'UX'),
	(14 ,'Radosław'   ,'JANKOWSKI'   ,  27 ,'UX'),
	(15 ,'Patryk'     ,'LEWANDOWSKI' ,  28 ,'Tester'),
	(16 ,'Patryk'     ,'ZIELIŃSKI'   ,  28 ,'Tester'),
	(17 ,'Andrzej'    ,'WOŹNIAK'     ,  31 ,'Tester'),
	(18 ,'Andrze'     ,'LEWANDOWSKI' ,  30 ,'Tester'),
	(19 ,'Roman'      ,'ZIELIŃSKI'   ,  39 ,'Tester'),
	(20 ,'Ewa'        ,'WOŹNIAK'     ,  31 ,'Tester');
    
-- 4
SELECT imie, nazwisko FROM pracownicy2
WHERE wiek>30;
    
-- 5
SELECT imie, nazwisko FROM pracownicy2
WHERE wiek<30;
    
-- 6
SELECT * FROM pracownicy2
WHERE nazwisko LIKE "K%ki";

-- 7
ALTER TABLE pracownicy2
RENAME COLUMN ID TO NR;

-- 8
SELECT 
*FROM pracownicy2
WHERE NR IS NULL;
SELECT 
*FROM pracownicy2
WHERE Imie IS NULL;
SELECT 
*FROM pracownicy2
WHERE Nazwisko IS NULL;
SELECT 
*FROM pracownicy2
WHERE Kurs IS NULL;

-- Nie jestem pewna czy jest to poprawne, nie znalazłam innego rozwiązania

-- 9 
SELECT Imie, Nazwisko FROM pracownicy2
WHERE Kurs = "Java";
